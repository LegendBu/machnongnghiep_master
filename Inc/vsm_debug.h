
#ifndef DEBUG_H_
#define DEBUG_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Qualcomm QCA4010 */
// #include <qcom/qcom_common.h>

/* Realtek RTL8720 */
//#include "diag.h"

#if defined(__gnu_linux__)
#define COMPILER_GCC
#endif


#define VSM_DEBUG_DISABLE                0
#define VSM_DEBUG_ERROR_DISABLE          0
#define VSM_DEBUG_WARN_DISABLE           0
#define VSM_DEBUG_INFO_DISABLE           0
#define VSM_DEBUG_RAW_DISABLE            0



#undef VSM_DEBUG
#undef VSM_DEBUG_ERROR
#undef VSM_DEBUG_WARN

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"


void debug_nothing(const char* sz, ...);
// void debug_print(const char* string, ...);
/* QCA4010 */
//#define debug_print         qcom_printf
/* Realtek RTL8720 */
#define debug_print         printf

// void debug_print_raw(const char* string, ...);
void debug_dump(const void* data, int len, const char* string, ...);




#define VSM_DEBUG_DUMP                           debug_dump

// #define VSM_DEBUG_RAW(s, args...)                debug_print(s, ##args)
// #define VSM_DEBUG(s, args...)                    debug_print(KWHT "[D] %s::%d " s "\n" KNRM, __FILE__, __LINE__, ##args)
// #define VSM_DEBUG_INFO(s, args...)               debug_print(KGRN "[I] %s " s "\n" KNRM, __FILE__, ##args)
// #define VSM_DEBUG_ERROR(s, args...)              debug_print(KRED "[E] %s " s "\n" KNRM, __FILE__, ##args)
// #define VSM_DEBUG_WARN(s, args...)               debug_print(KYEL "[W] %s " s "\n" KNRM, __FILE__, ##args)
// #define VSM_DEBUG_COLOR(color, s, args...)       debug_print(color s "\n" KNRM, ##args)

#define VSM_DEBUG_RAW(s, args...)                debug_print(s, ##args)
#define VSM_DEBUG(s, args...)                    debug_print(KWHT "[D] " s "\n" KNRM, ##args)
#define VSM_DEBUG_INFO(s, args...)               debug_print(KGRN "[I] " s "\n" KNRM, ##args)
#define VSM_DEBUG_ERROR(s, args...)              debug_print(KRED "[E] " s "\n" KNRM, ##args)
#define VSM_DEBUG_WARN(s, args...)               debug_print(KYEL "[W] " s "\n" KNRM, ##args)
#define VSM_DEBUG_COLOR(color, s, args...)       debug_print(color s "\n" KNRM, ##args)



#if (VSM_DEBUG_DISABLE == 1)
#undef VSM_DEBUG
#define VSM_DEBUG            debug_nothing
#endif
#if (VSM_DEBUG_ERROR_DISABLE == 1)
#undef VSM_DEBUG_ERROR
#define VSM_DEBUG_ERROR      debug_nothing
#endif
#if (VSM_DEBUG_WARN_DISABLE == 1)
#undef VSM_DEBUG_WARN
#define VSM_DEBUG_WARN    debug_nothing
#endif
#if (VSM_DEBUG_INFO_DISABLE == 1)
#undef VSM_DEBUG_INFO
#define VSM_DEBUG_INFO       debug_nothing
#endif
#if (VSM_DEBUG_RAW_DISABLE == 1)
#undef VSM_DEBUG_RAW
#define VSM_DEBUG_RAW        debug_nothing
#endif

// #define VSM_DEBUG

/* Define ASSERT MACROS */
#ifndef ASSERT_NONVOID
    #define ASSERT_NONVOID(con,ret)             {if(!(con)) {VSM_DEBUG_ERROR("\r\nASSERT in file %s, line %d\r\n", __FILE__, __LINE__); return ret;} }
#endif

#ifndef ASSERT_VOID
    #define ASSERT_VOID(con)                    {if(!(con)) {VSM_DEBUG_ERROR("\r\nASSERT in file %s, line %d\r\n", __FILE__, __LINE__); return;   }  }
#endif

#ifndef ASSERT
    #define ASSERT(con)                         {if(!(con)) {VSM_DEBUG_ERROR("\r\nASSERT in file %s, line %d\r\n", __FILE__, __LINE__);           }  }
#endif




#ifdef __cplusplus
}
#endif

#endif /* DEBUG_H_ */
