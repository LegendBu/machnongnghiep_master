#ifndef __PC_CONTROL_H__
#define __PC_CONTROL_H__

#include "stm32f1xx_hal.h"
#include "main.h"
#include "usart.h"
#include "control.h"
typedef enum
{
    NODE_NO_CMD =0,
    NODE_STATUS,
    NODE_NUMBER,
    NODE_CHECK,
    NODE_CONFIG_ADDRESS,
    NODE_FAULT_STATE,
    NODE_MAXIMUM
} node_event_t;

typedef void (*fnNodeCallback)(node_event_t, uint16_t);
typedef uint8_t(*nodeReadFn)(uint8_t*);

void nodeInit(void);

void nodeProcessLoop(void* p);

void nodeSetReadFunc();

void nodeEventRegCallback(node_event_t event, fnNodeCallback cb);

node_event_t checkCmdFromPC(void);

#endif
