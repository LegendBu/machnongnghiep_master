#ifndef __STATUS_H
#define __STATUS_H

#include "stm32f1xx_hal.h"
#include "main.h"


#define MAX_SENSORS   10
#define MAX_NODES     10
typedef enum
{
  SENSOR_DISABLE  = 0,
  SENSOR_ENABLE   = 1
}SENSOR_BOOL;
typedef enum
{
  NODE_DISABLE  = 0,
  NODE_ENABLE   = 1
}NODE_BOOL;
typedef enum
{
  DUST_SENSOR  = 0,
  HUMI_SENSOR  = 1,
  TEMP_SENSOR  = 2
}SENSOR_TYPE;
typedef struct
{
    uint8_t sensorAdress;
    uint16_t dataSensor;
    SENSOR_BOOL isAvalible;
}SENSOR_Status;
typedef struct
{
    uint8_t nodeAdress;
    uint8_t numberSensor;
    NODE_BOOL isAvalible;
    SENSOR_Status sensorArr[MAX_SENSORS];
}NODE_Status;
typedef struct
{
    uint8_t numberNodeConected;
    NODE_Status NodeArr[MAX_NODES];
}NODE;


void initial(void);
#endif /**/
