#ifndef __DUST_SENSOR_H__
#define __DUST_SENSOR_H__

#include "stm32f1xx_hal.h"
#include "main.h"
#include "usart.h"
typedef enum
{
    DUST_SENSOR_UPDATE = 0,
    DUST_SENSOR_TEMP_ERROR,
    DUST_SENSOR_FAN_ERROR,
    DUST_SENSOR_LAZE_ERROR,
    DUST_SENSOR_MAX
} sensor_event_t;

typedef void (*fnCallback)(sensor_event_t, uint16_t);
typedef uint8_t(*readFn)(uint8_t*);

void dustSensorInit(void);

void dustSensorProcessLoop(void* p);

void dustSensorSetReadFunc();

void dustSensorEventRegCallback(sensor_event_t event, fnCallback cb);

void getData_Sesor(sensor_event_t event, uint16_t * value);

#endif
