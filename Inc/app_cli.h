#ifndef _APP_CLI__H
#define _APP_CLI__H

#include "vsm_shell.h"

// typedef int32_t (*cmd_function_t)(p_shell_context_t ctx context, int32_t argc, char **argv);

void app_cli_init(void);

int32_t app_cli_status(p_shell_context_t ctx, int32_t argc, char** argv);
int32_t app_cli_check(p_shell_context_t ctx, int32_t argc, char** argv);
int32_t app_cli_search(p_shell_context_t ctx, int32_t argc, char** argv);
int32_t app_cli_reset(p_shell_context_t ctx, int32_t argc, char** argv);
int32_t app_cli_dust(p_shell_context_t ctx, int32_t argc, char** argv);
int32_t app_cli_humidity(p_shell_context_t ctx, int32_t argc, char** argv);
int32_t app_cli_temp(p_shell_context_t ctx, int32_t argc, char** argv);
void app_cli_task(void* p);
#endif
