#include "status.h"

NODE nodeList;

//Lay ra trang thai co bn sensor ket noi
void initial(void)
{
  nodeList.numberNodeConected = MAX_NODES;
  for (uint8_t i = 0; i < MAX_NODES; i++)
  {
  	nodeList.NodeArr[i].nodeAdress = i+1;
    nodeList.NodeArr[i].numberSensor = MAX_SENSORS;
    nodeList.NodeArr[i].isAvalible = NODE_ENABLE;
    for (uint8_t j = 0; j < MAX_SENSORS; j ++)
    {
      nodeList.NodeArr[i].sensorArr[j].isAvalible = SENSOR_ENABLE;
      nodeList.NodeArr[i].sensorArr[j].sensorAdress = j+1;
      nodeList.NodeArr[i].sensorArr[j].dataSensor = j + 0x49;
    }
  }
}
