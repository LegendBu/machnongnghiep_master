#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <ctype.h>
#include "app_cli.h"
#include "usart.h"
#include "vsm_debug.h"
#include "status.h"

shell_context_struct shell_ctx;
extern NODE nodeList;
static const shell_command_context_t cli_command_table[] = {
	{"reset",       "\"reset 1\": reset node by input address 1\r\n",                                      app_cli_reset,               1},
	{"status",       "\"status 1\": status of node 1. Return all sensor values of this node\r\n",          app_cli_status,              1},
	{"search",       "\"search\": Check new node is pending or not\r\n",                                   app_cli_search,              0},
	{"check",       "\"check 1\": Return the number of sensor of node\r\n",                                app_cli_check,               1},
	{"dust",       "\"dust 1\": Return the value of dust sensor of node\r\n",                              app_cli_dust,                1},
	{"humidity",       "\"dust 1\": Return the value of humidity sensor of node\r\n",                      app_cli_humidity,            1},
	{"temp",       "\"dust 1\": Return the value of temp sensor of node\r\n",                              app_cli_temp,                1},
};

int putchar (int c)
{
  /* Write a character to the UART2 */
  HAL_UART_Transmit(&huart1, (uint8_t *)&c, 1, 1);
  return c;
}

int send_callback(uint8_t* buf, uint32_t len)
{
	HAL_UART_Transmit(&huart1, buf, len, 0xFFFF);
    return 0;
}

int recv_callback(uint8_t* buf, uint32_t len)
{
    if(__HAL_UART_GET_FLAG(&huart1,UART_FLAG_RXNE) ==  1) {
      buf[0] = (uint8_t)(huart1.Instance->DR & (uint16_t)0x00FF);
      return 1;
    }
    buf[0] = 0;
    return 0;
}

void app_cli_task(void* p)
{
	SHELL_Process((p_shell_context_t)&shell_ctx);
}

void app_cli_init(void)
{
	SHELL_Init((p_shell_context_t)&shell_ctx, send_callback, recv_callback, printf, ">");

    for(int i = 0; i < sizeof(cli_command_table)/sizeof(shell_command_context_t); i ++) {
        SHELL_RegisterCommand(&cli_command_table[i]);
    }
}

int32_t app_cli_status(p_shell_context_t ctx, int32_t argc, char** argv)
{
	if(isdigit(*argv[1]))
	{
		uint32_t  node_addr           = atoi(argv[1]);
    	uint8_t data[70];
    	uint8_t id =0;
    	if(node_addr != 0 && node_addr <= MAX_NODES)
    	{
			for(uint8_t i =0; i < MAX_SENSORS;i++)
			{
				if(nodeList.NodeArr[node_addr-1].sensorArr[i].isAvalible == SENSOR_ENABLE)
    		    {
    		      data[id++] = nodeList.NodeArr[node_addr-1].sensorArr[i].sensorAdress / 10 + 0x30;
    		      data[id++] = nodeList.NodeArr[node_addr-1].sensorArr[i].sensorAdress % 10 + 0x30;
    		      data[id++] = 0x20;
    		      data[id++] = nodeList.NodeArr[node_addr-1].sensorArr[i].dataSensor / 100 + 0x30;
    		      data[id++] = (nodeList.NodeArr[node_addr-1].sensorArr[i].dataSensor / 10) % 10 + 0x30;
    		      data[id++] = nodeList.NodeArr[node_addr-1].sensorArr[i].dataSensor % 10 + 0x30;
    		      data[id++] = 0x20;
    		    }
			}
			VSM_DEBUG("Node addr[%d], Value: %s", node_addr, data);
		}
		else
			VSM_DEBUG("Have no this address node");
	}
	else
		VSM_DEBUG("parameter is not a numberic");
    return 0;
}

int32_t app_cli_check(p_shell_context_t ctx, int32_t argc, char** argv)
{
	if(isdigit(*argv[1]))
	{
		uint32_t  node_addr           = atoi(argv[1]);
		if(node_addr <= MAX_NODES)
		{
			if(node_addr == 0)
			{
    		  VSM_DEBUG("app_cli_check Number nodes conected in network: %d", nodeList.numberNodeConected);
			}
    		else
    		{
    		  VSM_DEBUG("app_cli_check Node addr[%d] Number sensor in node : %d", node_addr,nodeList.NodeArr[node_addr-1].numberSensor);
    		}
    	}
    	else
    		VSM_DEBUG("Wrong parameter");
	}
	else
		VSM_DEBUG("parameter is not a numberic");
    return 0;
}

int32_t app_cli_search(p_shell_context_t ctx, int32_t argc, char** argv)
{
	VSM_DEBUG("app_cli_search");

    return 0;
}

int32_t app_cli_reset(p_shell_context_t ctx, int32_t argc, char** argv)
{
	uint32_t  node_addr           = atoi(argv[1]);

	VSM_DEBUG("Reset node [%d]", node_addr);

    return 0;
}
int32_t app_cli_dust(p_shell_context_t ctx, int32_t argc, char** argv)
{
	if(isdigit(*argv[1]))
	{
		uint32_t  node_addr           = atoi(argv[1]);
		if(node_addr != 0 && node_addr <= MAX_NODES)
		{
			VSM_DEBUG("app_cli_dust Dust sensor Value in node[%d] : %d", node_addr, nodeList.NodeArr[node_addr-1].sensorArr[DUST_SENSOR].dataSensor);
		}
		else
			VSM_DEBUG("Have no this address node");
	}
	else
		VSM_DEBUG("parameter is not a numberic");
    return 0;
}
int32_t app_cli_humidity(p_shell_context_t ctx, int32_t argc, char** argv)
{
	if(isdigit(*argv[1]))
	{
		uint32_t  node_addr           = atoi(argv[1]);
		if(node_addr != 0 && node_addr <= MAX_NODES)
		{
			VSM_DEBUG("app_cli_humidity humidity sensor Value node [%d] : %d", node_addr, nodeList.NodeArr[node_addr-1].sensorArr[HUMI_SENSOR].dataSensor);
		}
		else
			VSM_DEBUG("Have no this address node");
	}
	else
		VSM_DEBUG("parameter is not a numberic");
    return 0;
}
int32_t app_cli_temp(p_shell_context_t ctx, int32_t argc, char** argv)
{
	if(isdigit(*argv[1]))
	{
		uint32_t  node_addr           = atoi(argv[1]);
		if(node_addr != 0 && node_addr <= MAX_NODES)
		{
			VSM_DEBUG("app_cli_temp temp sensor Value node [%d] : %d", node_addr, nodeList.NodeArr[node_addr-1].sensorArr[TEMP_SENSOR].dataSensor);
		}
		else
			VSM_DEBUG("Have no this address node");
	}
	else
		VSM_DEBUG("parameter is not a numberic");
    return 0;
}
