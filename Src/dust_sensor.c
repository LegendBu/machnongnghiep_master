#include "dust_sensor.h"
#include "status.h"
fnCallback gDustSenSorCallback[DUST_SENSOR_MAX];
readFn gDustSensorReadByte = NULL;
uint8_t state = 0;
uint8_t buff[28];
uint16_t value_sensor =0;
extern NODE nodeList;
void dustSensorInit(void)
{
    uint16_t i = 0;

    for(i = 0; i < DUST_SENSOR_MAX; i++)
    {
        gDustSenSorCallback[i] = NULL;
    }
}
static uint8_t checkSum(uint8_t* Buff,uint8_t size)
{
  uint16_t i=0,sum =0;
  for(i=0;i<(size-2);i++)
  {
    sum += Buff[i];
  }
  sum+= 0xAB;
  if(((sum >> 8) == Buff[size-2]) && ((sum & 0xFF) == Buff[size-1]))
    return 1;
  else
    return 0;
}

// ham ni dat trong while(1)
void dustSensorProcessLoop(void* p)
{
    uint8_t ch = 0;
    static uint8_t id =0;
    if (gDustSensorReadByte != NULL)
    {
       if(gDustSensorReadByte(&ch) == 0)
       {
        return;
       }
    }
    else
    {
        return;
    }
    switch(state)
    {
        case 0: // doi header 1
            if (ch == 0x42)
            {
                state++;
            }
            break;
        case 1:
            if (ch == 0x4d)
            {
                state++;
            }
            else
            {
                state = 0; // quay ve state 1
            }
            break;
        case 2: // cho byte length 1
            if(ch == 0x00)
            {
                state++;
            }
            else
            {
                state = 0; // quay ve state 1
            }
            break;
        case 3: // cho length 2
            if(ch == 0x1C)
            {
                state++;
            }
            else
            {
                state = 0; // quay ve state 1
            }
            break;
        case 4:
             buff[id] = ch;
            id++;
            if(id>=27)
            {
               state++;
            }
            break;
        case 5: // cho check sum2, tinh toan checksum, neu dung thi goi cac callback tuong ung ra
            {
              buff[id] = ch;
               if(checkSum(buff,28))
               {
                    value_sensor = (uint16_t)(buff[2])*256 + buff[3];
                    nodeList.NodeArr[0].sensorArr[DUST_SENSOR].dataSensor = value_sensor;
                    if (gDustSenSorCallback[DUST_SENSOR_UPDATE] != NULL)
                    {
                        gDustSenSorCallback[DUST_SENSOR_UPDATE](DUST_SENSOR_UPDATE, value_sensor);
                    }
//                    if (buff[25] == LOWTEMP && gDustSenSorCallback[DUST_SENSOR_TEMP_ERROR] != NULL)
//                    {
//                       gDustSenSorCallback[DUST_SENSOR_TEMP_ERROR](NULL);
//                    }
//                    if (buff[25] == LOWTEMP && gDustSenSorCallback[DUST_SENSOR_TEMP_ERROR] != NULL)
//                    {
//                       gDustSenSorCallback[DUST_SENSOR_TEMP_ERROR](NULL);
//                    }
//                    if (buff[25] == LOWTEMP && gDustSenSorCallback[DUST_SENSOR_TEMP_ERROR] != NULL)
//                    {
//                       gDustSenSorCallback[DUST_SENSOR_TEMP_ERROR](NULL);
//                    }
               }
               state =0;
               id =0;
               break;
            }

    }
}
void dustSensorSetReadFunc(readFn cb)
{
    if (cb != NULL)
    {
        gDustSensorReadByte = cb;
    }
}

void dustSensorEventRegCallback(sensor_event_t event, fnCallback cb)
{
    if (NULL != cb)
    {
        gDustSenSorCallback[event] = cb;
    }
}
