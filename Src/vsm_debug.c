
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>

#include <vsm_debug.h>


/**
 * @brief      Do nothing
 *
 * @param[in]  sz         The size
 * @param[in]  <unnamed>  { parameter_description }
 */
void debug_nothing(const char* sz, ...){

}

/**
 * @brief      Dump memory data array
 *
 * @param[in]  data       The data
 * @param[in]  len        The length
 * @param[in]  string     The banner string
 * @param[in]  args       std args
 */
void debug_dump(const void* data, int len, const char* string, ...)
{
    uint8_t* p = (uint8_t*)data;
    uint8_t  buffer[16];
    int iLen;
    int i;

    debug_print("%s %u bytes\n", string, len);
    while (len > 0)
    {
        iLen = (len > 16) ? 16 : len;
        memset(buffer, 0, 16);
        memcpy(buffer, p, iLen);
        for (i = 0; i < 16; i++)
        {
            if (i < iLen)
                debug_print("%02X ", buffer[i]);
            else
                debug_print("   ");
        }
        debug_print("\t");
        for (i = 0; i < 16; i++)
        {
            if (i < iLen)
            {
                if (isprint(buffer[i]))
                    debug_print("%c", (char)buffer[i]);
                else
                    debug_print(".");
            }
            else
                debug_print(" ");
        }
        debug_print("\n");
        len -= iLen;
        p += iLen;
    }
}





