#include "pc_control.h"
fnNodeCallback gNodeCallback[NODE_MAXIMUM];
nodeReadFn gNodeReadByte = NULL;
static uint8_t state1 = 0;
static uint8_t numberBackSlash = 0;
static uint8_t buff1[20];
uint16_t status=0;
void nodeInit(void)
{
    uint16_t i = 0;

    for(i = 0; i < NODE_MAXIMUM; i++)
    {
        gNodeCallback[i] = NULL;
    }
}
//static uint8_t checkSum(uint8_t* Buff,uint8_t size)
//{
//  uint16_t i=0,sum =0;
//  for(i=0;i<(size-2);i++)
//  {
//    sum += Buff[i];
//  }
//  sum+= 0xC3;
//  sum+= nodeAdd;
//  if(((sum >> 8) == Buff[size-2]) && ((sum & 0xFF) == Buff[size-1]))
//    return 1;
//  else
//    return 0;
//}
node_event_t checkCmdFromPC(void)
{
  if((buff1[0] == 'S') && (buff1[1] == 'T') && (buff1[2] == 'A') && (buff1[3] == 'T') && (buff1[4] == 'U') && (buff1[5] == 'S'))
  {
    return NODE_STATUS;
  }
  return NODE_NO_CMD;
}
// ham ni dat trong while(1)
void nodeProcessLoop(void* p)
{
    uint8_t ch = 0;
    static uint8_t id =0;
    if (gNodeReadByte != NULL)
    {
       if(gNodeReadByte(&ch) == 0)
       {
        return;
       }
    }
    else
    {
        return;
    }
    switch(state1)
    {
        case 0: // doi header 1
            if (ch == 'N')
            {
                state1++;
            }
            break;
        case 1:
            if (ch == 'O')
            {
                state1++;
            }
            else
            {
                state1 = 0; // quay ve state1 1
            }
            break;
        case 2:
            if (ch == 'D')
            {
                state1++;
            }
            else
            {
                state1 = 0; // quay ve state1 1
            }
            break;
        case 3:
            if (ch == 'E')
            {
                state1++;
            }
            else
            {
                state1 = 0; // quay ve state1 1
            }
            break;
        case 4: // check adress
            if(ch == ' ')
            {
                state1++;
            }
            else
            {
                state1 = 0; // quay ve state1 1
            }
            break;
        case 5: // nhan du lieu
            buff1[id] = ch;
            id++;
            if(ch == 0x5C)
            {
              if(++numberBackSlash == 2)
              {
                state1++;
                numberBackSlash =0;
              }
            }
            break;
        case 6: // cho check sum2, tinh toan checksum, neu dung thi goi cac callback tuong ung ra
            {
              buff1[id] = ch;
              if ((checkCmdFromPC() == NODE_STATUS) && gNodeCallback[NODE_STATUS] != NULL)
              {
                  gNodeCallback[NODE_STATUS](NODE_STATUS, status);
              }
               state1 =0;
               id =0;
               break;
            }

    }
}
void nodeSetReadFunc(nodeReadFn cb)
{
    if (cb != NULL)
    {
        gNodeReadByte = cb;
    }
}

void nodeEventRegCallback(node_event_t event, fnNodeCallback cb)
{
    if (NULL != cb)
    {
        gNodeCallback[event] = cb;
    }
}
